##
# Miscellaneous helper functions
##

#from numpy import *
import numpy as np

def random_weight_matrix(m, n):
    #### YOUR CODE HERE ####
    
    epsilon = np.sqrt(6)/(np.sqrt(m + n))
    A0 = 2*epsilon*np.random.rand(m, n)-epsilon

    #### END YOUR CODE ####
    assert(A0.shape == (m,n))
    return A0